<?php

/**
 * @file
 * Contains \Drupal\bootstrap_gallery_if\Plugin\Field\FieldFormatter\BootstrapGalleryImageFormatter.
 */

namespace Drupal\bootstrap_gallery_if\Plugin\Field\FieldFormatter;

use Drupal\image\Plugin\Field\FieldFormatter\ImageFormatterBase;
use Drupal\Core\Entity\EntityStorageInterface;
use Drupal\Core\Field\FieldItemListInterface;
use Drupal\Core\Field\FieldDefinitionInterface;
use Drupal\Core\Plugin\ContainerFactoryPluginInterface;
use Drupal\Core\Session\AccountInterface;
use Drupal\Core\Url;
use Drupal\Core\Utility\LinkGeneratorInterface;
use Symfony\Component\DependencyInjection\ContainerInterface;
use Drupal\Core\Form\FormStateInterface;
use Drupal\Core\Cache\Cache;
use Drupal\image\Entity\ImageStyle;

/**
 * Plugin implementation of the 'bootstrap_gallery_image_formatter' formatter.
 *
 * @FieldFormatter(
 *   id = "bootstrap_gallery_image_formatter",
 *   label = @Translation("Bootstrap Gallery"),
 *   field_types = {
 *     "image"
 *   }
 * )
 */
class BootstrapGalleryImageFormatter extends ImageFormatterBase implements ContainerFactoryPluginInterface {
  /**
   * The current user.
   *
   * @var \Drupal\Core\Session\AccountInterface
   */
  protected $currentUser;

  /**
   * The link generator.
   *
   * @var \Drupal\Core\Utility\LinkGeneratorInterface
   */
  protected $linkGenerator;

  /**
   * The image style entity storage.
   *
   * @var \Drupal\Core\Entity\EntityStorageInterface
   */
  protected $imageStyleStorage;

  /**
   * Constructs an ImageFormatter object.
   *
   * @param string $plugin_id
   *   The plugin_id for the formatter.
   * @param mixed $plugin_definition
   *   The plugin implementation definition.
   * @param \Drupal\Core\Field\FieldDefinitionInterface $field_definition
   *   The definition of the field to which the formatter is associated.
   * @param array $settings
   *   The formatter settings.
   * @param string $label
   *   The formatter label display setting.
   * @param string $view_mode
   *   The view mode.
   * @param array $third_party_settings
   *   Any third party settings settings.
   * @param \Drupal\Core\Session\AccountInterface $current_user
   *   The current user.
   * @param \Drupal\Core\Utility\LinkGeneratorInterface $link_generator
   *   The link generator service.
   * @param \Drupal\Core\Entity\EntityStorageInterface $image_style_storage
   *   The entity storage for the image.
   */
  public function __construct($plugin_id, $plugin_definition, FieldDefinitionInterface $field_definition, array $settings, $label, $view_mode, array $third_party_settings, AccountInterface $current_user, LinkGeneratorInterface $link_generator, EntityStorageInterface $image_style_storage) {
    parent::__construct($plugin_id, $plugin_definition, $field_definition, $settings, $label, $view_mode, $third_party_settings);
    $this->currentUser = $current_user;
    $this->linkGenerator = $link_generator;
    $this->imageStyleStorage = $image_style_storage;
  }

  /**
   * {@inheritdoc}
   */
  public static function create(ContainerInterface $container, array $configuration, $plugin_id, $plugin_definition) {
    return new static(
      $plugin_id,
      $plugin_definition,
      $configuration['field_definition'],
      $configuration['settings'],
      $configuration['label'],
      $configuration['view_mode'],
      $configuration['third_party_settings'],
      $container->get('current_user'),
      $container->get('link_generator'),
      $container->get('entity.manager')->getStorage('image_style')
    );
  }

  /**
   * {@inheritdoc}
   */
  public static function defaultSettings() {
    return array(
      'image_style' => '',
      'image_style_full' => '',
      'use_modal' => 1,
      'extra' => 0,
      'image_bootstrap_style' => '',
    ) + parent::defaultSettings();
  }

  /**
   * {@inheritdoc}
   */
  public function settingsForm(array $form, FormStateInterface $form_state) {
    $image_styles = image_style_options(FALSE);
    $element['image_style'] = array(
      '#title' => t('Image style thumbnail'),
      '#type' => 'select',
      '#default_value' => $this->getSetting('image_style'),
      '#empty_option' => t('None (original image)'),
      '#options' => $image_styles,
      '#description' => array(
        '#markup' => $this->linkGenerator->generate($this->t('Configure Image Styles'), new Url('entity.image_style.collection')),
        '#access' => $this->currentUser->hasPermission('administer image styles'),
      ),
    );
    $element['image_style_full'] = array(
      '#title' => t('Image style full'),
      '#type' => 'select',
      '#default_value' => $this->getSetting('image_style_full'),
      '#empty_option' => t('None (original image)'),
      '#options' => $image_styles,
      '#description' => array(
        '#markup' => $this->linkGenerator->generate($this->t('Configure Image Styles'), new Url('entity.image_style.collection')),
        '#access' => $this->currentUser->hasPermission('administer image styles'),
      ),
    );
    $element['image_bootstrap_style'] = array(
      '#title' => t('Image bootstrap style'),
      '#type' => 'checkboxes',
      '#default_value' => $this->getSetting('image_bootstrap_style'),
      '#description' => t('Add classes to an <code>&lt;img&gt;</code> element to easily style images in any project.'),
      '#empty_option' => t('None'),
      '#options' => array('img-rounded' => t('Rounded'), 'img-circle' => t('Circle'), 'img-thumbnail' => t('Thumbnail')),
    );
    $element['use_modal'] = array(
      '#title' => t('Use modal'),
      '#type' => 'select',
      '#options' => array(0 => 'no', 1 => 'yes'),
      '#default_value' => $this->getSetting('use_modal'),
      '#description' => t('Enables the original borderless layout.'),
    );
    $element['extra'] = array(
      '#title' => t('Settings'),
      '#type' => 'select',
      '#options' => array('0' => t('- None -'), 'controls' => t('Controls'), 'carousel' => t('Carousel')),
      '#description' => t('Initialize the Gallery with visible controls or display the images in an inline carousel instead of a lightbox.'),
      '#default_value' => $this->getSetting('extra'),
    );
    return $element;
  }

   /**
    * {@inheritdoc}
    */
  public function settingsSummary() {
    $summary = array();
    $image_styles = image_style_options(FALSE);

    // Unset possible 'No defined styles' option.
    unset($image_styles['']);

    // Styles could be lost because of enabled/disabled modules that defines
    // their styles in code.
    $image_style = $this->getSetting('image_style');
    if (isset($image_styles[$image_style])) {
      $summary[] = t('Image thumbnail style: @style', array('@style' => $image_styles[$image_style]));
    }
    else {
      $summary[] = t('Original image as thumbnail');
    }
    $image_style_full = $this->getSetting('image_style_full');
    if (isset($image_styles[$image_style_full])) {
      $summary[] = t('Image full style: @style', array('@style' => $image_styles[$image_style_full]));
    }
    else {
      $summary[] = t('Original image as full');
    }

    if ($this->getSetting('use_modal') == 0) {
      $summary[] = t('No modal');
    }
    else {
      $summary[] = t('With modal');
    }

    switch ($this->getSetting('extra')) {
      case 'controls':
        $summary[] = t('With controls');
        break;
      case 'carousel':
        $summary[] = t('Without lightbox');
        break;
    }
    return $summary;
  }

  /**
   * {@inheritdoc}
   */
  public function viewElements(FieldItemListInterface $items, $langcode) {
    $images = array();
    $files = $this->getEntitiesToView($items, $langcode);

    // Early opt-out if the field is empty.
    if (empty($files)) {
      return $images;
    }

    $image_style_setting = $this->getSetting('image_style');
    $image_style_setting_full = $this->getSetting('image_style_full');

    // Collect cache tags to be added for each item in the field.
    $cache_tags = array();
    if (!empty($image_style_setting)) {
      $image_style = $this->imageStyleStorage->load($image_style_setting);
      $cache_tags = $image_style->getCacheTags();
    }

    foreach ($files as $delta => $file) {
      $image_uri = $file->getFileUri();
      $url = ImageStyle::load($image_style_setting_full)->buildUrl($image_uri);

      $cache_tags = Cache::mergeTags($cache_tags, $file->getCacheTags());

      // Extract field item attributes for the theme function, and unset them
      // from the $item so that the field template does not re-render them.
      $item = $file->_referringItem;
      $item_attributes = $item->_attributes;
      unset($item->_attributes);

      $extra['class'] = $this->getSetting('image_bootstrap_style');

      $images[$delta] = array(
        'title' => $items[$delta]->getValue()['title'],
        'url' => $url,
        'thumb' => array(
          '#theme' => 'image_formatter',
          '#item' => $item,
          '#item_attributes' => $item_attributes + $extra,
          '#image_style' => $image_style_setting,
          '#cache' => array(
            'tags' => $cache_tags,
          ),
        ),
      );
    }

    // Build theme array.
    $element[0] = array(
      '#theme' => 'bootstrap_gallery',
      '#images' => $images,
      '#use_modal' => $this->getSetting('use_modal'),
      '#controls' => $this->getSetting('extra') == 'controls' ? 1 : 0,
      '#carousel' => $this->getSetting('extra') == 'carousel' ? 1 : 0,
    );
    if (!$this->getSetting('use_modal')) {
      $element[0]['#attributes']['data-use-bootstrap-modal'] = 'false';
    }

    return $element;
  }

}
